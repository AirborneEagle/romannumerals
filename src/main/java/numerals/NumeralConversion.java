/**
 * 
 */
package numerals;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author tyler
 *
 */
public class NumeralConversion
{
    private static String HUNDRED = "C";
    private static String FIFTY = "L";
    private static String TEN = "X";
    private static String FIVE = "V";
    private static String ONE = "I";
    
    /**
     * @param args
     * 
     * This method will parse the file located here: /home/tyler/Desktop/numFile
     * NOTE : lines must not contain any letters or the program will fail
     */
    public static void main(String[] args)
    {
        String filePath = "/home/tyler/Desktop/numFile";
        if(filePath != null)
        {
            try
            {
                int caseNum = 1;
                int numOfCases = 0;
                Scanner s = new Scanner(new File(filePath));
                ArrayList<Integer> numList = new ArrayList<Integer>();
                while (s.hasNextLine()){
                    numList.add(Integer.parseInt(s.nextLine()));
                }
                s.close();
                numOfCases = numList.get(0);
                while(caseNum <= numOfCases)
                {
                    int num = numList.get(caseNum);
                    if(num > 0 && num < 255)
                    {
                        System.out.println("Case #" + caseNum + ":" + num + "=" + getRomanVersion(num));
                    }
                    else
                    {
                        System.out.println("Case #" + caseNum + ":" + num + "=NA");
                    }
                    caseNum++;
                }
            }
            catch (Exception problem)
            {
                problem.printStackTrace();
            }
        }
    }

    /**
     * @param num the number which is to be converted into a roman numeral
     * num must be an integer between 1 - 255
     * @return a String representing the roman numeral version of num
     */
    private static String getRomanVersion(int num)
    {
        String rom = "";
        while(num >= 100)
        {
            rom+=HUNDRED;
            num = num-100;
        }
        while(num >= 50)
        {
            rom+=FIFTY;
            num = num-50;
        }
        while(num >= 10)
        {
            if(num >= 40)
            {
                rom+=TEN+FIFTY;
                num = num-40;
            }
            else
            {
                rom+=TEN;
                num = num-10;
            }
        }
        while(num >= 5)
        {
            rom+=FIVE;
            num = num-5;
        }
        while(num >= 1)
        {
            if(num >= 4)
            {
                rom+=ONE+FIVE;
                num = num-4;
            }
            else
            {
                rom+=ONE;
                num = num-1;
            }
        }
        return rom;
    }
}
